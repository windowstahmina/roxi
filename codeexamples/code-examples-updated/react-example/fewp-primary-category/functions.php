<?php

if (!function_exists('fewp_primary_cat_link')) {
    /**
     * Get primary category link
     *
     * @param int|\WP_Post $post
     * @return string
     */
    function fewp_primary_cat_link($post = 0)
    {
        return \FEWPCategory\Category::getInstance()
            ->getPermalink($post);
    }
}

if (!function_exists('fewp_primary_cat')) {
    /**
     * Get primary category object
     *
     * @param int|\WP_Post $post
     * @return \WP_Term|bool
     */
    function fewp_primary_cat($post = 0)
    {
        return \FEWPCategory\Category::getInstance()
            ->getPrimaryCategory($post);
    }
}

if (!function_exists('fewp_primary_cat_name')) {
    /**
     * Get primary category name
     *
     * @param int|\WP_Post $post
     * @return string
     */
    function fewp_primary_cat_name($post = 0)
    {
        return \FEWPCategory\Category::getInstance()
            ->getName($post);
    }
}

if (!function_exists('fewp_primary_cat_ID')) {
    /**
     * Get primary category ID
     *
     * @param int|\WP_Post $post
     * @return int
     */
    function fewp_primary_cat_ID($post=0)
    {
        return \FEWPCategory\Category::getInstance()
            ->getID($post);
    }
}


if (!function_exists('fewp_primary_category')) {
    /**
     * Display category name with the link when called inside the loop
     * or global $post is available.
     * 
     * @param int|\WP_Post $post
     * @return void
     */
    function fewp_the_primary_category($post=0)
    {

        echo \FEWPCategory\Category::getInstance()->getTheCategory($post);
    }
}
