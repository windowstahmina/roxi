import { useState, useEffect } from "react";

(function(wp) {
  const registerPlugin = wp.plugins.registerPlugin;
  const PluginSidebar = wp.editPost.PluginSidebar;
  const el = wp.element.createElement;
  const SelectControl = wp.components.SelectControl;
  const withSelect = wp.data.withSelect;
  const withDispatch = wp.data.withDispatch;
  const compose = wp.compose.compose;
  const { select } = wp.data;

  const MetaBlockField = compose(
    withDispatch(function(dispatch, props) {
      return {
        setMetaFieldValue: function(value) {
          dispatch("core/editor").editPost({
            meta: { [props.fieldName]: value },
          });
        },
      };
    }),
    withSelect(function(select, props) {
      return {
        metaFieldValue: select("core/editor").getEditedPostAttribute("meta")[
          props.fieldName
        ],
      };
    })
  )(function(props) {
    const initialSate = [{
      label: '-',
      value: ''
    }];
    const [categories, setCategories] = useState([{}]);
  
    useEffect(() => {
      wp.apiFetch({ path: "/wp/v2/categories" })
        // 'terms' contains valid term objects
        .then((terms) => {
          let selectedCategory = select("core/editor").getEditedPostAttribute(
            "categories"
          );
          let options = initialSate;

          terms.forEach(function(term, index) {
            if (selectedCategory.includes(term.id)) {
              let option = {
                label: term.name,
                value: term.id,
              };

              options.sort(function(a, b) {
                const catA = a.label.toUpperCase();
                const catB = b.label.toUpperCase();

                let comparison = 0;
                if (catA > catB) {
                  comparison = 1;
                } else if (catA < catB) {
                  comparison = -1;
                }
                return comparison;
              });
              options.push(option);
            }
          });
          setCategories(options);
        });
    }, []);
    //props.metaFieldValue
    return el(SelectControl, {
      label: "Select Primary Category",
      value: props.metaFieldValue,
      options: categories,
      onChange: function(content) {
        props.setMetaFieldValue(content);
      },
    }); //end
  });

  registerPlugin("fewp-category-sidebar", {
    render: function() {
      const [hasTaxonomy, setHasTaxonomy] = useState(false);
      useEffect(() => {
        wp.apiFetch({ path: "/wp/v2/types" })
          // 'terms' contains valid term objects
          .then((types) => {
            let currentPostType = wp.data
              .select("core/editor")
              .getCurrentPostType();
            Object.entries(types).forEach(([key, value]) => {
              if (
                currentPostType === key &&
                value.taxonomies.includes("category")
              ) {
                setHasTaxonomy(true);
              }
            });
          });
      }, []);

      if (!hasTaxonomy) {
        return null;
      }

      return el(
        PluginSidebar,
        {
          name: "fewp-category-sidebar",
          icon: "smiley",
          title: "Primary Category",
        },
        el(
          "div",
          { className: "fewp-category-content", style: { padding: "1rem" } },
          el(MetaBlockField, { fieldName: "fewp_primary_category" })
        )
      );
    },
  });
})(window.wp);
