<?php

/**
 * Plugin Name: Primary Category for WordPress posts/ Custom Posts
 * Plugin URI: https://bitbucket.org/tahminachowdhury/inpsyde-custom-plugin/
 * Description: User can able to set primary category for the posts/ custom posts
 *
 * Version: 1.0.0
 * Author: Fazle Elahee
 * Author URI: http://tchowdhury.com/
 **/

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

//set plugin name
define('FEWPC_PLUGIN_NAME', 'fewp-category');

//set plugin url
define("FEWPC_PLUGIN_URL", trailingslashit(plugin_dir_url(__FILE__)));

//set base directory
define("FEWPC_PLUGIN_BASE_PATH", __DIR__);

/**
 * Function for getting plugin class
 *
 * phpcs:disable Inpsyde.CodeQuality.ReturnTypeDeclaration.NoReturnType
 * phpcs:disable NeutronStandard.Globals.DisallowGlobalFunctions.GlobalFunctions
 */
function fewpc_plugin()
{
    static $plugin;

    if (null !== $plugin) {
        return $plugin;
    }

    if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 70400) {
        return null;
    }

    $pluginClassName = 'FEWPCategory\Plugin';
    /** @var FEWPCategory\Plugin $plugin */

    if (!class_exists($pluginClassName)) {
        /**
         * Register namespace with SPL auto loader, instead of adding file individually.
         * Its more scalable and safest options I think.
         */
        include_once "wpcp-autoload-register.php";
    }

    include_once __DIR__.'/functions.php';

    $plugin = new $pluginClassName();
    $plugin->init();
    return $plugin;
}

/**
 * Run
 */
if (function_exists('add_action')) {
    add_action('plugins_loaded', 'fewpc_plugin');
}
