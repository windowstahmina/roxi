<?php

declare(strict_types=1);

namespace FEWPCategory;

class Plugin
{
    /**
     * Plugin Version
     *
     * @var string
     */
    public const VERSION = '1.0.0';


    public const PRIMARY_CATEGORY_META = 'fewp_primary_category';


    /**
     * Init all actions and filters
     */
    public function init(): Plugin
    {
        $this->addHooks();
        return $this;
    }

    /**
     *  Trigger WordPress hooks when plugins loaded.
     *
     * @return void
     */
    public function addHooks(): void
    {
        add_action('init', [$this, 'registerPostMeta']);
        add_action('init', [$this, 'registerSideBarAssets']);
        add_action('enqueue_block_editor_assets', [$this, 'enqueueEditorAssets']);
    }

    /**
     * To define/ regiere custom post meta
     *
     * @return void
     */
    public function registerPostMeta(): void
    {
        $excludeTypes = [
            'page', 'wp_block', 'attachment', 'revision', 'nav_menu_item', 'custom_css',
            'customize_changeset', 'oembed_cache', 'user_request'
        ];

        $postTypes = get_post_types([], 'name');

        foreach($postTypes  as $key => $postType) {
            if(!in_array($key,  $excludeTypes) && ($key == 'post' || in_array('category', $postType->taxonomies))) {
                register_post_meta($key, self::PRIMARY_CATEGORY_META, array(
                    'show_in_rest' => true,
                    'single' => true,
                    'type' => 'string',
                ));
            }
        }
    }

    /**
     * Register editor plugin side bar
     *
     * @return void
     */
    public function registerSideBarAssets(): void
    {
        $asset_file = include(FEWPC_PLUGIN_BASE_PATH .  '/assets/build/index.asset.php');

        wp_register_script(
            'fewp-primary-category-admin',
            FEWPC_PLUGIN_URL . 'assets/build/index.js',
            $asset_file['dependencies'] + array('wp-plugins', 'wp-edit-post', 'wp-element'),
            $asset_file['version'],
            true
        );
    }

    /**
     * Load Assets
     * 
     * @return void
     */
    public function enqueueEditorAssets(): void
    {
        wp_enqueue_script('fewp-primary-category-admin');
    }
}
