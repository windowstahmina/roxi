<?php

namespace FEWPCategory;

final class Category
{
    private static $instance = null;

    /**
     * gets the instance via lazy initialization (created on first usage)
     */
    public static function getInstance(): Category
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Get primary category from WordPress global $post var
     * or override by passing postID or post object  
     *
     * @param int|\WP_Post $post
     * @return \WP_Term category object
     */
    public function getPrimaryCategory($post = 0)
    {
        $post = get_post($post);

        if (!isset($post->ID)) {
            return false;
        }

        $catgories = get_the_category( $post->ID );

        $catIdsInPost = array_map(fn($category) => $category->term_id, $catgories);
        $catID = get_post_meta($post->ID, Plugin::PRIMARY_CATEGORY_META, true);

        if (!$catID || !in_array($catID, $catIdsInPost)) {
            if($catID) {
                // if the catgory not attached with post, 
                // this category can not be a primary category for this post.
                delete_post_meta($post->ID, Plugin::PRIMARY_CATEGORY_META);
            }
            return false;
        }

        return get_category($catID);
    }

    /**
     * Get permalink of rhe current post or 
     * given the posID±post object
     *
     * @param int|WP_Post $post
     * @return string
     */
    public function getPermalink($post = 0)
    {
        $category = self::getInstance()->getPrimaryCategory($post);
        if (!$category) {
            return false;
        }
        return get_category_link($category);
    }

    /**
     * Get category name of rhe current post or 
     * given the posID±post object
     *
     * @param int|\WP_Post $post
     * @return string
     */
    public function getName($post = 0)
    {
        $category = self::getInstance()->getPrimaryCategory($post);
        return isset($category->name) ?? '';
    }

    /**
     * Get category ID of rhe current post or 
     * given the posID±post object
     *
     * @param int|\WP_Post $post
     * @return int|bool
     */
    public function getID($post = 0)
    {
        $category = self::getInstance()->getPrimaryCategory($post);
        return isset($category->term_id) ?? false;
    }

    /**
     * Generate href tag with permalink of the current post or
     * given the posID±post object 
     *
     * @param int|\WP_Post $post
     * @return string
     */
    public function getTheCategory($post = 0)
    {
        global $wp_rewrite;
        $category = self::getInstance()->getPrimaryCategory($post);

        if (!$category) {
            return '';
        }

        $rel = (is_object($wp_rewrite) && $wp_rewrite->using_permalinks()) ? 'rel="category tag"' : 'rel="category"';

        $output = '<a href="' . esc_url(get_category_link($category->term_id)) . '"  ' . $rel . '>';
        if ($category->parent) {
            $output .= get_category_parents($category->parent, false, '');
        }
        $output .= $category->name . '</a>';
        return apply_filters('fewp_the_primary_category', $output);
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct()
    {
    }
}
